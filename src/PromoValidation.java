public abstract class PromoValidation {

    private DiscountType discountType;
    private double discount;

    public PromoValidation(DiscountType discountType, double discount) {
        this.discountType = discountType;
        this.discount = discount;
    }

    abstract boolean isApplicable(double amount);

    public double calculateDiscount(double amount) {
        if (discountType.equals(DiscountType.AMOUNT)) {
            return amount - discount;
        } else {
            return amount - (amount * discount * 0.01);
        }
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
