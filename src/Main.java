import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Category food = new Category("food");

        Product apple = new Product("Apple", 100.0, food);
        Product almond = new Product("Almond", 150.0, food);

        DeliveryCostCalculator deliveryCostCalculator = new DeliveryCostCalculator(2.0 ,3.0);
        ShoppingCart shoppingCart = new ShoppingCart(deliveryCostCalculator);

        shoppingCart.addItem(almond, 3);
        shoppingCart.addItem(apple, 2);

        Campaign campaign = new Campaign(DiscountType.RATE, 10, 3, food);
        Coupon coupon = new Coupon(DiscountType.AMOUNT, 10, 100);

        List<Campaign> campaignList = new ArrayList<>();
        campaignList.add(campaign);

        shoppingCart.applyDiscount(campaignList);
        shoppingCart.applyCoupon(coupon);

        System.out.println(shoppingCart.toString());
    }
}
