public class Coupon extends PromoValidation {

    private double minCartTotalPrice;

    public Coupon(DiscountType discountType, double discount, double minCartTotalPrice) {
        super(discountType, discount);
        this.minCartTotalPrice = minCartTotalPrice;
    }

    public double getMinCartTotalPrice() {
        return minCartTotalPrice;
    }

    public void setMinCartTotalPrice(double minCartTotalPrice) {
        this.minCartTotalPrice = minCartTotalPrice;
    }

    @Override
    public boolean isApplicable(double amount) {
        return amount >= minCartTotalPrice;
    }
}
