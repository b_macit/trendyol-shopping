import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShoppingCart {

    private double totalPrice;
    private Map<Product, Integer> productQuantity;
    private double totalAppliedDiscountPrice;
    private double totalAppliedCouponPrice;
    private DeliveryCostCalculator deliveryCostCalculator;

    public ShoppingCart(DeliveryCostCalculator deliveryCostCalculator) {
        this.deliveryCostCalculator = deliveryCostCalculator;
        this.productQuantity = new HashMap<>();
    }

    public void addItem(Product product, int quantity) {
        if (product == null) {
            throw new IllegalArgumentException("Product cannot be empty");
        }
        if (quantity <= 0) {
            throw new IllegalArgumentException("Invalid quantity, amount cannot be less than 1 " + quantity);
        }

        if (this.productQuantity.containsKey(product))
            this.productQuantity.replace(product, quantity);
        else
            this.productQuantity.put(product, quantity);
    }

    public double applyDiscount(List<Campaign> campaignList) {
        Map<Category, List<Product>> categoryProductListMap = mappingCategoryProducts();

        double discountedCartPrice = 0.0;
        for (Campaign campaign : campaignList) {
            List<Product> products = categoryProductListMap.get(campaign.getCategory());
            for(Product product : products) {
                if (campaign.isApplicable(productQuantity.get(product))) {
                    double productPrice = calculateProductsPrice(product);
                    double discountedProductPrice = campaign.calculateDiscount(productPrice);
                    this.totalAppliedDiscountPrice += productPrice - discountedProductPrice;
                    discountedCartPrice += discountedProductPrice;
                } else {
                    discountedCartPrice += calculateProductsPrice(product);
                }
            }
        }
        this.totalPrice = discountedCartPrice;
        return this.totalPrice;
    }

    public double applyCoupon(Coupon coupon) {
        if(coupon.isApplicable(this.totalPrice)){
            double appliedCouponCartPrice = coupon.calculateDiscount(this.totalPrice);
            this.totalAppliedCouponPrice = this.totalPrice - appliedCouponCartPrice;
            this.totalPrice = appliedCouponCartPrice;
        }
        return this.totalPrice;
    }

    public Map<Category, List<Product>> mappingCategoryProducts() {
        return productQuantity.keySet().stream().collect(Collectors.groupingBy(Product::getCategory));
    }

    private Double calculateCartPrice() {
        this.totalPrice = calculateProductsPrice(new ArrayList<>(productQuantity.keySet()));
        return this.totalPrice;
    }

    private Double calculateProductsPrice(Product product) {
        return product.getPrice() * productQuantity.get(product);
    }

    private Double calculateProductsPrice(List<Product> productList) {
        return productList.stream().mapToDouble(p -> productQuantity.get(p) * p.getPrice()).sum();
    }

    public double getTotalAmountAfterDiscount() {
        return calculateCartPrice() - this.totalAppliedCouponPrice - totalAppliedDiscountPrice;
    }

    public double getCouponDiscount() {
        return this.totalAppliedCouponPrice;
    }

    public double getCampaignDiscount() {
        return this.totalAppliedDiscountPrice;
    }

    public double getDeliveryCost() {
        return deliveryCostCalculator.calculateFor(this);
    }

    public Map<Product, Integer> getProductQuantity() {
        return productQuantity;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "totalPrice=" + totalPrice +
                ", productQuantity=" + productQuantity +
                ", totalAppliedDiscountPrice=" + totalAppliedDiscountPrice +
                ", totalAppliedCouponPrice=" + totalAppliedCouponPrice +
                ", deliveryCostCalculator=" + deliveryCostCalculator +
                '}';
    }
}
