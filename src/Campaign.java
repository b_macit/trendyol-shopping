public class Campaign extends PromoValidation {

    private int minQuantity;
    private Category category;

    public Campaign(DiscountType discountType, double discount, int minQuantity, Category category) {
        super(discountType, discount);
        this.minQuantity = minQuantity;
        this.category = category;
    }

    public int getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(int minQuantity) {
        this.minQuantity = minQuantity;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean isApplicable(double amount) {
        return amount >= getMinQuantity();
    }
}
