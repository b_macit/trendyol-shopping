public class DeliveryCostCalculator {

    private double costPerDelivery;
    private double costPerProduct;
    private final double FIXED_COST = 2.99;

    public DeliveryCostCalculator(double costPerDelivery, double costPerProduct) {
        this.costPerDelivery = costPerDelivery;
        this.costPerProduct = costPerProduct;
    }

    public double calculateFor(ShoppingCart cart) {
        double numberOfDeliveries = cart.mappingCategoryProducts().keySet().size();
        double numberOfProducts = cart.getProductQuantity().keySet().size();

        return (costPerDelivery * numberOfDeliveries) + (costPerProduct * numberOfProducts) + FIXED_COST;
    }
}
