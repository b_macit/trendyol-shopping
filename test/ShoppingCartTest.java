import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ShoppingCartTest {

    private ShoppingCart shoppingCart;
    private Product apple;
    private Product almond;
    private Product laktaseFreeMilk;
    private Product soyMilk;
    private Category food;
    private Category milk;

    @Before
    public void setUp() {
        food = new Category("food");
        milk = new Category("milk", food);

        apple = new Product("Apple", 100.0, food);
        almond = new Product("Almond", 150.0, food);
        laktaseFreeMilk = new Product("Laktase Free Milk", 3.49, milk);
        soyMilk = new Product("Soy Milk", 4.49, milk);

        DeliveryCostCalculator deliveryCostCalculator = new DeliveryCostCalculator(2.0 ,3.0);
        shoppingCart = new ShoppingCart(deliveryCostCalculator);

        shoppingCart.addItem(almond, 3);
        shoppingCart.addItem(apple, 2);
        shoppingCart.addItem(laktaseFreeMilk, 3);
        shoppingCart.addItem(soyMilk, 2);
    }

    @Test
    public void givenProductWithoutDiscountWhenGetTotalAmountAfterDiscountThenItReturnsTotalPriceInCart() {

        double totalCartPrice = shoppingCart.getTotalAmountAfterDiscount();

        assertEquals(669.45, totalCartPrice, 0.01);
    }

    @Test
    public void givenProductWithDiscountAndCouponWhenGetTotalAmountAfterDiscountCallThenItReturnsTotalPriceInCart() {

        Campaign campaign = new Campaign(DiscountType.RATE, 10, 3, food);
        Campaign campaign2 = new Campaign(DiscountType.RATE, 10, 3, milk);
        Coupon coupon = new Coupon(DiscountType.AMOUNT, 10, 100);

        List<Campaign> campaignList = new ArrayList<>();
        campaignList.add(campaign);
        campaignList.add(campaign2);

        shoppingCart.applyDiscount(campaignList);
        shoppingCart.applyCoupon(coupon);

        double totalCartPrice = shoppingCart.getTotalAmountAfterDiscount();

        assertEquals(613.40, totalCartPrice, 0.01);
    }

    @Test
    public void givenProductDiscountAndCouponWhenGetCouponDiscountCallThenReturnsAppliedCouponInCart(){

        Campaign campaign = new Campaign(DiscountType.RATE, 10, 3, food);
        Campaign campaign2 = new Campaign(DiscountType.RATE, 10, 3, milk);
        Coupon coupon = new Coupon(DiscountType.AMOUNT, 10, 100);

        List<Campaign> campaignList = new ArrayList<>();
        campaignList.add(campaign);
        campaignList.add(campaign2);

        shoppingCart.applyDiscount(campaignList);
        shoppingCart.applyCoupon(coupon);

        double couponDiscount = shoppingCart.getCouponDiscount();

        assertEquals(10, couponDiscount, 0.01);
    }

    @Test
    public void givenProductDiscountAndCouponWhenGetCouponDiscountCallThenItReturnsAppliedDiscountInCart(){

        Campaign campaign = new Campaign(DiscountType.RATE, 10, 3, food);
        Campaign campaign2 = new Campaign(DiscountType.RATE, 10, 3, milk);

        List<Campaign> campaignList = new ArrayList<>();
        campaignList.add(campaign);
        campaignList.add(campaign2);

        shoppingCart.applyDiscount(campaignList);

        double campaignDiscount = shoppingCart.getCampaignDiscount();

        assertEquals(46.04, campaignDiscount, 0.01);
    }

    @Test
    public void givenProductWithDiffrentCategoriesWhenGetDeliveryCostCallThenItReturnsDeliveryCostInCart() {
        double deliveryCost = shoppingCart.getDeliveryCost();

        assertEquals(18.99, deliveryCost, 0.01);
    }
}
